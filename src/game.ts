////////////////////////////////////////////////////////////////////////////
////////////////////////put models in scene/////////////////////////////////

import utils from "../node_modules/decentraland-ecs-utils/index"

const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floorBaseGrass_01 = new Entity()
floorBaseGrass_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape)
const transform_10 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(2, 1, 2)
})
floorBaseGrass_01.addComponentOrReplace(transform_10)
engine.addEntity(floorBaseGrass_01)

const ball5 = new Entity
const gltfShape2 = new GLTFShape ('models/balls/ball5.glb')
ball5.addComponentOrReplace(gltfShape2)
const transform_6 = new Transform ({
  position: new Vector3(0, 1, 8),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
ball5.addComponentOrReplace(transform_6)
engine.addEntity(ball5)

const lemur = new Entity
const gltfShape3 = new GLTFShape ('models/lemur/lemur_v3.glb')
lemur.addComponentOrReplace(gltfShape3)
const transform_9 = new Transform ({
  position: new Vector3(12, 0, 12),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
lemur.addComponentOrReplace(transform_9)
engine.addEntity(lemur)

const ball6 = new Entity
const gltfShape4 = new GLTFShape ('models/balls/ball6.glb')
ball6.addComponentOrReplace(gltfShape4)
const transform_7 = new Transform ({
  position: new Vector3(-2, 1, 8),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
ball6.addComponentOrReplace(transform_7)
engine.addEntity(ball6)

const ball1 = new Entity
const gltfShape5 = new GLTFShape ('models/balls/ball1.glb')
ball1.addComponentOrReplace(gltfShape5)
const transform_1 = new Transform ({
  position: new Vector3(8, 1, 1),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
ball1.addComponentOrReplace(transform_1)
engine.addEntity(ball1)

const ball2 = new Entity()
const gltfShape6 = new GLTFShape ('models/balls/ball2.glb')
ball2.addComponentOrReplace(gltfShape6)
const transform_2 = new Transform ({
  position: new Vector3(6, 1, 1),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
ball2.addComponentOrReplace(transform_2)
engine.addEntity(ball2)

const ball3 = new Entity()
const gltfShape7 = new GLTFShape ('models/balls/ball3.glb')
ball3.addComponentOrReplace(gltfShape7)
const transform_4 = new Transform ({
  position: new Vector3(4, 1, 8),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
ball3.addComponentOrReplace(transform_4)
engine.addEntity(ball3)

const ball4 = new Entity
const gltfShape8 = new GLTFShape ('models/balls/ball4.glb')
ball4.addComponentOrReplace(gltfShape8)
const transform_5 = new Transform ({
  position: new Vector3(2, 1, 8),
  rotation: new Quaternion(0, 0, 0, 0),
  scale: new Vector3(1, 1, 1)
})
ball4.addComponentOrReplace(transform_5)
engine.addEntity(ball4)



